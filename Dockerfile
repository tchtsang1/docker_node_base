FROM node:6

# Default system timezone (Hong kong)
ENV TIMEZONE=GMT+08
# Used to configure OS language support
ENV LANGUAGE=zh_HK.UTF-8

RUN chmod a+rwx /
RUN mkdir /.npm && chmod a+rwx /.npm && chmod a+rwx /usr/local/lib/node_modules
USER root

